#!/usr/bin/env python
import re
import sys
from datetime import datetime
from enum import Enum
import json


""" Text substituted for omitted media """
MEDIA_TEXT = "<Media omitted>"

class MessageTypes(Enum):
    NORMAL = 0
    MEDIA = 1
    MULTILINE = 2
    SYSTEM = 4

def read_file(path):
    """ Read file into list of lines """

    with open(path) as f:
        lines = f.readlines()

    return lines

def parse_line(line):
    """ Parse a single line of input """

    # Single-line message or first line of multi-line message
    default = re.compile(r'(?P<date>\d?\d\/\d?\d\/\d{2}), (?P<time>\d{2}:\d{2}) - (?P<author>[\w\- ]+)\: (?P<message>.+\n)')
    # System message such as group joins etc. Only applies if the message doesn't match default pattern.
    system = re.compile(r'(?P<date>\d?\d\/\d?\d\/\d{2}), (?P<time>\d{2}:\d{2}) - (?P<message>.+\n)')

    msg_default = default.match(line)
    msg_system = system.match(line)

    if msg_default is not None:
        # Message is a single-line of first line of multi-line.
        msg_dict = msg_default.groupdict()

        # Convert date and time strings to datetime.datetime object for handling.
        msg_dict["datetime"] = datetime.strptime("{date}T{time}".format(**msg_dict),"%m/%d/%yT%H:%M")

        # Set type to MEDIA if text matches MEDIA_TEXT
        if msg_dict.get("first_line") == MEDIA_TEXT:
            return (MessageTypes.MEDIA, msg_dict)
        # Normal message. Type will be updated to MULTILINE if next parsed message is part of multiline.
        else:
            return (MessageTypes.NORMAL, msg_dict)
    elif msg_system is not None:
        # Message does not match default pattern, but matches system pattern (Date&Time, but no author)
        msg_dict = msg_system.groupdict()

        return (MessageTypes.SYSTEM, msg_dict)
    else:
        # If message matches neither Default nor System pattern, it is assumed to be a multiline
        # continuation of a previous message
        return (MessageTypes.MULTILINE, {"line": line})


def parse_list(lines):
    """ Parse list of lines """
    msgs = []

    for line in lines:
        # Parse the line, get type and content.
        current_type, current_dict = parse_line(line)

        if current_type == MessageTypes.NORMAL or current_type == MessageTypes.MEDIA:
            # No specific treatment for MEDIA messages yet.
            #TODO: Optionally check agains a provided folder containing images?
            msgs.append(current_dict)
        if current_type == MessageTypes.MULTILINE:
            # Multiline messages are assumed to be a continuation of the previously parsed message.
            # If no preceeding message exists, line is malformed and will be dropped
            if len(msgs) < 1:
                continue

            # Last pessage gets popped from list, amended with new line, then added to list again.
            msg = msgs.pop()
            msg["message"] += current_dict.get("line","")
            msgs.append(msg)

    return msgs

def parse_file(path):
    """ Read and fully parse a file """

    lines = read_file(path)

    return parse_list(lines)

def json_datetime_handler(x):
    """ Datetime handler for the JSON export. Serializes datetime.datetime as a ISO 8601 Datetime string.
        Timezone is not recorded """

    if isinstance(x, datetime):
        return x.isoformat()
    raise TypeError("Unknown type")

def export_json_file(messages, output_file):
    """ Export a list of message dicts to json """

    with open(output_file,'w', encoding='utf8') as fout:
        json.dump(messages, fout, default=json_datetime_handler, ensure_ascii=False, indent=4)


def main():
    args = sys.argv

    help = """WhatsApp to JSON converter
    Usage: parse.py <input file> <output file>"""

    try:
        if args[1] == '--help' or args[1] == '?':
            print(help)
        else:
            msgs = parse_file(args[1])
            export_json_file(msgs, args[2])
    except IndexError as e:
        print(help)
    except PermissionError as e:
        print("{0}: {1}".format(e.strerror, e.filename))
    except:
        raise

if __name__ == "__main__":
    main()

