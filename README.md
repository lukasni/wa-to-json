WhatsApp to JSON
================

This module takes a whatsapp log as generated by WhatsApp's "Email chat" functionality and parses it into JSON.
